from django.contrib import admin
from django.urls import path
from ninja import NinjaAPI

api = NinjaAPI()


@api.get("/hello")
def add(request):
    return {"result": 'Hello World'}
